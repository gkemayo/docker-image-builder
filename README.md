# Docker Image Builder

L'objectif de ce mini projet est de proposer un pipeline GitLab CI qui automatise entièrement le build, le tag et le push d'une image docker dans un registry (Dockerhub, Quay.io, ...) que le developpeur aura choisi. Le pipeline push aussi accésoirement l'image dans la registry GitLab du projet.<br/>

***Plus value :*** plus besoin d'avoir Docker sur sa machine pour builder une image. Il suffit de :
 - Forker ce projet dans son espace GitLab 
 - Remplacer le contenu du présent Dockerfile par son propre contenu 
 - Renseigner tous les paramètres concernant l'image (nom, tag, ...) ainsi que sa registry (username, password, url, ...) au démarrage du pipeline ou dans le fichier *.gitlab-ci.yml* directement.
 - Declencher le pipeline

 ***Résultat :*** Si le pipeline se termine avec succès, on retrouvera son image dans la registry indiquée.
